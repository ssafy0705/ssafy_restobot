# -*- coding: utf-8 -*-
import re
import urllib.request
import datetime
from selenium import webdriver
from bs4 import BeautifulSoup

from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter

cnt = 0
cnt_for_loc = 0

SLACK_TOKEN = "xoxb-689652854976-689653880896-lQLKswvzLpyenq9mdERjGfSW"
SLACK_SIGNING_SECRET = "dd4f2798246519a36aaa273260c62d54"




app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)


# 크롤링 함수 구현하기
def _crawl_naver_keywords(text):

    if "안녕" in text:
        return "안녕하세요 식당을 찾아드릴까요?"

    elif "식당" in text:
        return "저는 문열린 식당을 찾아드립니다. 원하는 장소를 말해주세요(동성로, 동대구 등)"

    else:
        # first_text = text.split('>')[1]
        # print(first_text)
        new_input = text.split('>')[1] + ' 맛집'
        print(new_input)
        slack_web_client.chat_postMessage(channel="#ssafy", text="잠시만 기다려주시면, 해당 시간에 열린 식당을 찾아드릴게요~")
        #크롬창 띄우기
        path = 'c:\chromedriver\chromedriver.exe'
        driver = webdriver.Chrome(path)
        print("크롬띄움")
        driver.implicitly_wait(3)
        #네이버 드가기
        url = 'http://www.naver.com'
        driver.get(url)
        print("네이버")
        #검색창에 치는거
        elem = driver.find_element_by_name('query')
        elem.clear()
        elem.send_keys(new_input)
        print("검색")
        #검색버튼 누르기
        driver.find_element_by_id('search_btn').click()
        url1 = driver.current_url
        source_code1 = urllib.request.urlopen(url1).read()
        soup = BeautifulSoup(source_code1, "html.parser")
        new_url = soup.find('div', class_='_nx_place_wrapper').find("div", class_="section_more").find("a")["href"]
        driver.get(new_url)

        hrefs = []
        li = driver.find_elements_by_css_selector('div.list_item_inner>a')
        for link in li:

            link.click()
            driver.switch_to.window(driver.window_handles[-1])
            hrefs.append(driver.current_url)
            driver.switch_to.window(driver.window_handles[0])
        print(hrefs) #링크
        list_title = []
        list_contents = []

        for resto_url in hrefs :
             # URL 주소에 있는 HTML 코드를 soup에 저장합니다.
            source_code3 = urllib.request.urlopen(resto_url).read()
            soup3 = BeautifulSoup(source_code3, "html.parser")

            list_content = soup3.find_all("div", class_="biztime")

            for news_title in list_content:
                list_title.append(news_title.find("span", class_="time").getText())


            #     list_title.sort(key=len)
            #     time = list_title[-1].split()[1]
            #     print(time)
            #
            #     comTime = datetime.datetime.strptime(str(time), "%H:%M")
            #
            #     print("ddfd")
            #     print(comTime)
            #
            #     my_time = input("시간을 말해주세요")
            #     my_time = datetime.datetime.strptime(str(my_time), "%H:%M")
            #     print(my_time)
            #
            #     if comTime <= my_time:
            #         print(list_title)
            #
            # list_contents.append(list_title)

        return list_title


def compare_time(list_data):

    list_data.sort(key=len)
    time = list_data[-1].split()[1]
    print(time)

    comTime = datetime.datetime.strptime(str(time), "%H:%M")

    print("ddfd")
    print(comTime)

    my_time = input("시간을 말해주세요")
    my_time = datetime.datetime.strptime(str(my_time), "%H:%M")
    print(my_time)

    if comTime <= my_time:
        print(list_data)






#챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):

    # attachments_dict = dict()
    # attachments_dict['pretext'] = "배고프세요? 원하는 시간에 열려있는 식당을 알려드립니다."
    # attachments_dict['title'] = "장소를 말해주세요(동성로, 동대구 등)"
    # attachments_dict['mrkdwn_in'] = ["text", "pretext"]  # 마크다운을 적용시킬 인자들을 선택합니다.
    # attachments = [attachments_dict]
    #
    # slack_web_client.chat.post_message(channel="#channel", text=None, attachments=attachments, as_user=True)
    #
    global cnt
    if cnt == 0:
        cnt += 1
        channel = event_data["event"]["channel"]
        print("channel")

        #slack_web_client.chat_postMessage(channel=channel, text="잠시만 기다려주시면, 해당 시간에 열린 식당을 찾아드릴게요~")
        text = event_data["event"]["text"]
        print("text")
        keywords = _crawl_naver_keywords(text)
        #slack_web_client.chat_postMessage(channel=channel, text="그래도 직접 검색하시는 것 보다는 제가 빠를꺼예요")
        #slack_web_client.chat_postMessage(channel=channel, text="이제 곧 결과가 나옵니다~~")
        print('keyword')
        slack_web_client.chat_postMessage(channel=channel, text=keywords)
        cnt = 0
        print("slack")




# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('127.0.0.1', port=5000)
